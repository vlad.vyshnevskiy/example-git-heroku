const bcrypt = require('bcrypt');
('use strict');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const email = 'test@gmail.com';
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash('xXx123reqw', salt);

    await queryInterface.bulkInsert(
      'users',
      [
        {
          email,
          password,
          createdAt: new Date(),
          updatedAt: new Date(),
          salt,
        },
      ],
      {}
    );
    const user = await queryInterface.sequelize.query(
      `SELECT id from Users WHERE email = '${email}'`
    );

    const UserId = user[0][0].id;

    await queryInterface.bulkInsert(
      'notes',
      [
        {
          title: 'title 1',
          text: 'text 1',
          slug: 'text-1',
          typeSharedLink: 'read',
          UserId,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: 'title 2',
          text: 'text 2',
          slug: 'text-2',
          typeSharedLink: 'change',
          UserId,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: 'title 3',
          text: 'text 3',
          slug: 'text-3',
          typeSharedLink: 'none',
          UserId,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', { email: 'test@gmail.com' }, {});
  },
};
