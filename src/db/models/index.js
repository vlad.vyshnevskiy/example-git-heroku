const Sequelize = require('sequelize').Sequelize;
const glob = require('glob');
const { config } = require('../../../config');

const sequelize = new Sequelize(config.db);

const factoriesModels = glob
  .sync(`${__dirname}/../../api/components/**/*.model.js`, { mark: true })
  .map((item) => {
    const { Factory } = require(item);
    return Factory;
  });

let models = {};
factoriesModels.forEach((item) => {
  const model = item(sequelize);
  models[model.name] = model;
});

Object.values(models)
  .filter((model) => typeof model.associate === 'function')
  .forEach((model) => model.associate(models));

const db = {
  ...models,
  sequelize,
  Sequelize,
};

(async () => {
  try {
    // await sequelize.sync();
    await sequelize.authenticate();
    console.log('Database connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
})();

module.exports = db;
