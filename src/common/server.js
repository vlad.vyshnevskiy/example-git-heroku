const Express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const http = require('http');
const cookieParser = require('cookie-parser');
const { config } = require('../../config');
const db = require('../db');

const errorHandler = require('../api/shared/middlewares/error.handler');

const app = new Express();

module.exports = class ExpressServer {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);

    app.set('appPath', `${root}client`);
    app.set('db', db);

    app.use(bodyParser.json({ limit: config.server.limit }));
    app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: config.server.limit,
      })
    );
    app.use(bodyParser.text({ limit: config.server.limit }));
    app.use(cookieParser(config.server.session_secret));
    app.use(Express.static(`${root}/public`));
  }

  router(routes) {
    this.routes = routes;
    return this;
  }

  listen(port = process.env.PORT) {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    this.routes(app);
    app.use(errorHandler);

    http.createServer(app).listen(port);

    return app;
  }
};
