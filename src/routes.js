const parserRoutes = require('./api/components/repository/repository.route');

module.exports = function routes(app) {
  app.use('/repository', parserRoutes);
};
