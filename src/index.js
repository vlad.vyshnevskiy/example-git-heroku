require('./common/env');
const Server = require('./common/server');
const routes = require('./routes');
const { config } = require('../config');

module.exports = new Server().router(routes).listen(config.server.port);
