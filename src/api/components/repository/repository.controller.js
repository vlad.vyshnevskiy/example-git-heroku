const asyncHandler = require('../../shared/middlewares/async.handler');
const { config } = require('../../../../config');
const { validationResult } = require('express-validator');
const {
  createRepositoriesOfHtml,
  findAllRepositories,
  updateRepository,
  findRepository,
  createRepository,
  deleteRepository,
} = require('./repository.service');

exports.loader = asyncHandler(async (req, res) => {
  await createRepositoriesOfHtml(config.url);

  return res.status(200).json({ success: true });
});

exports.create = asyncHandler(async (req, res) => {
  validationResult(req).throw();

  const repositories = await createRepository(req.body);

  return res.status(201).json({ success: true, repositories });
});

exports.update = asyncHandler(async (req, res) => {
  validationResult(req).throw();

  await updateRepository(req.body, req.params.name);

  return res.status(200).send({ success: true });
});

exports.get = asyncHandler(async (req, res) => {
  validationResult(req).throw();

  const repository = await findRepository(req.params.name);

  return res.status(200).send(repository);
});

exports.getAll = asyncHandler(async (req, res) => {
  const repositories = await findAllRepositories();

  return res.status(200).send(repositories);
});

exports.delete = asyncHandler(async (req, res) => {
  validationResult(req).throw();

  await deleteRepository(req.params.name);

  return res.status(204).send();
});
