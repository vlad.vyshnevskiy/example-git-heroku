const router = require('express').Router();
const controller = require('./repository.controller');
const { check, param } = require('express-validator');

router
  .route('/')
  .get(controller.getAll)
  .post(
    [
      check('name').not().isEmpty(),
      check('stars').not().isEmpty().isNumeric(),
      check('language').not().isEmpty(),
      check('forks').not().isEmpty().isNumeric(),
      check('description').not().isEmpty(),
    ],
    controller.create
  );

router.route('/download').get(controller.loader);

router
  .route('/:name')
  .get([param('name').not().isEmpty()], controller.get)
  .put(
    [
      check('name').not().isEmpty(),
      check('stars').not().isEmpty().isNumeric(),
      check('language').not().isEmpty(),
      check('forks').not().isEmpty().isNumeric(),
      check('description').not().isEmpty(),
    ],
    controller.update
  )
  .delete([param('name').not().isEmpty()], controller.delete);

module.exports = router;
