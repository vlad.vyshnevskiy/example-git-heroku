const { BaseModel } = require('../../base/base-column.model');
const { Model, DataTypes } = require('sequelize');

class Repository extends Model {}

module.exports = {
  Factory: (sequelize) => {
    Repository.init(
      {
        ...BaseModel,
        name: {
          unique: true,
          allowNull: false,
          type: DataTypes.STRING,
        },
        stars: {
          type: DataTypes.INTEGER,
        },
        language: {
          type: DataTypes.STRING,
        },
        forks: {
          type: DataTypes.INTEGER,
        },
        description: {
          allowNull: false,
          type: DataTypes.TEXT,
        },
      },
      {
        paranoid: true,
        sequelize,
        tableName: 'repositories',
      }
    );
    return Repository;
  },
};
