const { Repository } = require('../../../db');
const rp = require('request-promise');
const $ = require('cheerio');

async function createRepositoriesOfHtml(url) {
  const html = await getHtmlOfUrl(url);
  const repositories = parsingHtml(html);

  for (let i = 0; i < repositories.length; i++) {
    const rep = await Repository.findOne({
      where: { name: repositories[i].name },
    });

    if (rep)
      await Repository.update(repositories[i], { where: { id: rep.id } });
    else await Repository.create(repositories[i]);
  }
}

async function createRepository(repository) {
  try {
    return await Repository.create(repository);
  } catch (e) {
    if (e.parent.code === '23505')
      throw {
        message: `Name already exist`,
        statusCode: 409,
      };
    else
      throw {
        message: e.message,
        statusCode: e.code,
      };
  }
}

async function findRepository(name) {
  const rep = await Repository.findOne({ where: { name } });

  if (!rep)
    throw {
      message: `Not found repository by name:${name}`,
      statusCode: 404,
    };

  return rep;
}

async function findAllRepositories() {
  return await Repository.findAll();
}

async function updateRepository(repository, name) {
  const { id } = await findRepository(name);

  await Repository.update(repository, { where: { id } });
}

async function getHtmlOfUrl(url) {
  return await rp(url);
}

async function deleteRepository(name) {
  const rep = await findRepository(name);

  return await rep.destroy({ force: true });
}

function parsingHtml(html) {
  const data = [];

  $('.Box-row', html).each((i, e) => {
    data.push({
      name: $(e).children('h1').children('a')[0].children[4].data.trim(),
      stars: parseInt(
        $(e)
          .children('div.f6')
          .children('a')
          .first()
          .text()
          .trim()
          .replace(',', '')
      ),
      language: $('span[itemprop="programmingLanguage"]', e).text().trim(),
      forks: parseInt(
        $(e)
          .children('div.f6')
          .children('a')[1]
          .children[2].data.trim()
          .replace(',', '')
      ),
      description: $(e).children('p').text().trim(),
    });
  });

  return data;
}

module.exports = {
  findAllRepositories,
  updateRepository,
  createRepository,
  findRepository,
  createRepositoriesOfHtml,
  deleteRepository,
};
