const jwt = require('jsonwebtoken');
const { User } = require('../../../db');
const { config } = require('../../../../config');

exports.JWTVerifier = async (req, res, next) => {
  if (!req.headers.authorization) {
    return next({
      message: 'Unauthorized',
      statusCode: 401,
    });
  }

  const token = req.headers.authorization.replace('Bearer', '').trim();

  try {
    const decoded = jwt.verify(token, config.jwt.jwtSecrete);

    req.user = await User.findByPk(decoded.sub);
    next();
  } catch (err) {
    next({
      message: 'Unauthorized',
      statusCode: 401,
    });
  }
};
