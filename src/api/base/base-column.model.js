const { DataTypes } = require('sequelize');

module.exports = {
  BaseModel: {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
  },
};
