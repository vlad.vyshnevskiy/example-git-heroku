module.exports = {
  development: {
    username: 'postgres',
    password: 'xopek',
    database: 'parser',
    dialect: 'postgres',
  },
  test: {
    username: 'postgres',
    password: 'xopek',
    database: 'parser',
    dialect: 'postgres',
  },
  production: {
    url: process.env.DATABASE_URL,
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
