const fs = require('fs');
const configDB = JSON.parse(
  fs.readFileSync(`${__dirname}/../config-database-pg.json`)
);

module.exports = {
  server: {
    port: '4000',
    limit: process.env.REQUEST_LIMIT || '100kb',
    session_secret: process.env.SESSION_SECRET || 'secret',
  },
  db: {
    ...configDB.test,
    logging: false,
    define: {
      syncOnAssociation: true,
    },
    syncOnAssociation: true,
    seederStorage: 'sequelize',
  },
  jwt: {
    jwtSecrete: 'my-secret',
  },
  expressSesion: {
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
  },
};
