const _ = require('lodash');
const defaultConfig = require('./default.config');

const env = require(`./${process.env.NODE_ENV || 'development'}`.trim());
const config = _.merge({ ...defaultConfig }, env);

module.exports = { config };
