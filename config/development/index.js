const configDB = require('../config-database-pg')

module.exports = {
  server: {
    port: process.env.PORT || '3000',
    limit: process.env.REQUEST_LIMIT || '100kb',
    session_secret: process.env.SESSION_SECRET || 'secret',
  },
  db: {
    ...configDB.development,
    logging: false,
    define: {
      syncOnAssociation: true,
    },
    syncOnAssociation: true,
    seederStorage: 'sequelize',
  },
  jwt: {
    jwtSecrete: 'my-secret',
  },
  expressSesion: {
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
  },
};
